/*
 * TaskManager.h
 *
 * Created on: Apr 1, 2015
 *     Author: Jinqiang Cui
 * Modified on: May 19, 2015
 *     Author: Yuchao Hu
 * Modified on: Aug 16, 2017
 *     Author: Wayne Yong
 *
 * National Universtiy of Singapore
 */

#ifndef _TF_TO_POSESTAMPED_H_
#define _TF_TO_POSESTAMPED_H_

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include "nus_msgs/StateWithCovarianceStamped.h"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <vector>

class pubUavState
{
public:
    pubUavState();
    ~pubUavState();
    
private:

    void stateZCallback(const geometry_msgs::PoseStamped::ConstPtr& tfPtr);
    //void stateXYCallback(const nav_msgs::Odometry::ConstPtr& tfPtr);
    void stateXYCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& tfPtr);
    void ZEDstateXYCallback(const geometry_msgs::PoseStamped::ConstPtr& tfPtr);

private:
    ros::NodeHandle node_;
    ros::Timer main_loop_timer_;
    ros::Subscriber state_z_sub_;
    ros::Subscriber state_xy_sub_;
    ros::Publisher tf_pub_;
    nus_msgs::StateWithCovarianceStamped state_;
    bool only_use_zed;
};

#endif /* _TF_TO_POSESTAMPED_H_ */
