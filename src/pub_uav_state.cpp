/*
 * tfToPoseStamped.h
 *
 * Created on: Aug 14, 2018
 *     Author: Mingjie Lao

 * National Universtiy of Singapore
 */

#include <cmath>
#include <ros/package.h>
#include <tf/transform_datatypes.h>
#include "pub_uav_state.h"

#include "geometry_msgs/PoseWithCovariance.h"

// For debugging 

pubUavState::pubUavState()
{
  /* init ros */
  ros::NodeHandle private_nh("~");
  private_nh.param<bool>("only_use_zed", only_use_zed, false);
  state_z_sub_ = node_.subscribe("/mavros/position/local/sys_id_1", 1, &pubUavState::stateZCallback, this);
  if(only_use_zed)
    state_xy_sub_ = node_.subscribe("/zed/pose", 1, &pubUavState::ZEDstateXYCallback, this);
  else
    state_xy_sub_ = node_.subscribe("/state/measurement_xy", 1, &pubUavState::stateXYCallback, this);
  
  //state_xy_sub_ = node_.subscribe("/odom", 1, &pubUavState::stateXYCallback, this);
  tf_pub_ = node_.advertise<nus_msgs::StateWithCovarianceStamped>("/state/measurement", 1);
}

pubUavState::~pubUavState()
{
}

/* Local Position callback */
void pubUavState::stateZCallback(const geometry_msgs::PoseStamped::ConstPtr& tfPtr)
{
  state_.pose.pose.position.z = tfPtr->pose.position.z;
  // state_.pose.pose.orientation = tfPtr->pose.orientation;
}

//from zed
/*
void pubUavState::stateXYCallback(const nav_msgs::Odometry::ConstPtr& tfPtr)
{
  state_.header = tfPtr->header;
  state_.pose.pose.position.x = tfPtr->pose.pose.position.x;
  state_.pose.pose.position.y = tfPtr->pose.pose.position.y;
  state_.pose.pose.orientation = tfPtr->pose.pose.orientation;
  tf_pub_.publish(state_);
}
*/

//from ssf

void pubUavState::stateXYCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& tfPtr)
{
  state_.header = tfPtr->header;
  state_.pose.pose.position.x = tfPtr->pose.pose.position.x;
  state_.pose.pose.position.y = tfPtr->pose.pose.position.y;
  state_.pose.pose.orientation = tfPtr->pose.pose.orientation;
  tf_pub_.publish(state_);
}

void pubUavState::ZEDstateXYCallback(const geometry_msgs::PoseStamped::ConstPtr& tfPtr)
{
  ROS_INFO("use_zed_only!!!!!!!!!!!!!!!!!!!!!!");
  state_.header = tfPtr->header;
  state_.pose.pose.position.x = tfPtr->pose.position.x;
  state_.pose.pose.position.y = tfPtr->pose.position.y;
  state_.pose.pose.orientation = tfPtr->pose.orientation;
  tf_pub_.publish(state_);
}


//from msckf
// void pubUavState::stateXYCallback(const nav_msgs::Odometry::ConstPtr& tfPtr)
// {
//   state_.header = tfPtr->header;
//   state_.pose.pose.position.x = tfPtr->pose.pose.position.y;
//   state_.pose.pose.position.y = -tfPtr->pose.pose.position.x;
  
//   state_.pose.pose.orientation.x = tfPtr->pose.pose.orientation.x;
//   state_.pose.pose.orientation.y = tfPtr->pose.pose.orientation.y;
//   state_.pose.pose.orientation.z = tfPtr->pose.pose.orientation.z;
//   state_.pose.pose.orientation.w = tfPtr->pose.pose.orientation.w;

//   tf::Quaternion q(state_.pose.pose.orientation.x, state_.pose.pose.orientation.y, state_.pose.pose.orientation.z, state_.pose.pose.orientation.w);
//   tf::Matrix3x3 m(q);
//   double roll, pitch, yaw;
//   m.getRPY(roll, pitch, yaw);
//   std::cout << state_.pose.pose.position.x << "," << state_.pose.pose.position.y << "," << yaw << std::endl;
//   tf_pub_.publish(state_);
// }

