/*
 * tfToPoseStamped.h
 *
 * Created on: Aug 14, 2018
 *     Author: Mingjie Lao

 * National Universtiy of Singapore
 */

#include <cmath>
#include <ros/package.h>
#include <tf/transform_datatypes.h>
#include "TfToPoseStamped.h"
#include "nus_msgs/StateWithCovarianceStamped.h"
#include "geometry_msgs/PoseWithCovariance.h"
// For debugging 

TfToPoseStamped::TfToPoseStamped()
{
  /* init ros */
  ros::NodeHandle private_nh("~");

  tf_sub_ = node_.subscribe("/vicon/D_Drone/D_Drone", 1, &TfToPoseStamped::tfCallback, this);
  tf_pub_ = node_.advertise<nus_msgs::StateWithCovarianceStamped>("/vicon/pose", 1);
}

TfToPoseStamped::~TfToPoseStamped()
{
}

/* Local Position callback */
void TfToPoseStamped::tfCallback(const geometry_msgs::TransformStampedConstPtr& tfPtr)
{
  nus_msgs::StateWithCovarianceStampedPtr pose = boost::make_shared<nus_msgs::StateWithCovarianceStamped>();
  pose->header = tfPtr->header;
  pose->pose.pose.position.x = tfPtr->transform.translation.x;
  pose->pose.pose.position.y = tfPtr->transform.translation.y;

  tf_pub_.publish(pose);
}



