#include <ros/ros.h>
#include "pub_uav_state.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pub_uav_state");

    pubUavState t;
    
    ros::spin();
    
    return 0;
}
